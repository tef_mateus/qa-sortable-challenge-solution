require 'rubygems'
require 'capybara'
require 'capybara/dsl'

# The code below is related to the docker-compose file
# Capybara.register_driver :my_driver do |app|
#   Capybara::Selenium::Driver.new(
#     app,
#     browser: :chrome,
#     url: "http://#{ENV['SELENIUM_HOST']}:#{ENV['SELENIUM_PORT']}"
#     )
# end
# Capybara.app_host = "http://#{ENV['TEST_APP_HOST']}:#{ENV['TEST_PORT']}"

Capybara.current_driver = :selenium_chrome

module MyCapybaraTest
  class Test

    include Capybara::DSL

    def sort
      visit('localhost:3000')
      (0..5).each do |i|
        source = page.find('li', text: "Item #{i}")
        target = page.find("#app > ul > li:nth-child(#{i+1})")
        source.drag_to target
      end

      sleep(3000)

    end
  end
end

t = MyCapybaraTest::Test.new
t.sort