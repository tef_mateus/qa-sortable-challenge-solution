FROM node:8.13.0-jessie

ENV DEBIAN_FRONTEND noninteractive

EXPOSE 3000

RUN mkdir -m 770 -p /app
WORKDIR /app

ADD . ./

RUN npm install -g parcel \
    && npm install

ENTRYPOINT npm start