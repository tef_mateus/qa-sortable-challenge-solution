
# README

The proj is now wrapped in a docker container
To run the app use the following command:

```
docker-compose up --build
```

Now, if you go to [localhost:3000](http://localhost:3000), you should be able to see the app.

However I was unable to get the script with the `drag and drop sort` running inside the docker.
The docker will install everything needed to run the app, run the sort script on, but fail trying to run the script (hence why the last step on the script is commented).

To see the script running outside the docker you need to:

- Clone the repo
- Install [Ruby](https://github.com/ruby/ruby)
- Go into `Test` folder
- Run the command `gem install bundler`
- Run the command `bundle install`

and finally run the test with

```ruby
ruby mySort.rb
```